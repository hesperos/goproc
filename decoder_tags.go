package goproc

import "strings"

type tagSet map[string]string

func fromTagString(tagString string) tagSet {
	tags := strings.Split(tagString, ",")
	ts := make(tagSet)

	for _, tag := range tags {
		tag = strings.ToLower(tag)
		tag = strings.TrimSpace(tag)
		tagValue := ""
		tagSplit := strings.Split(tag, ":")
		if len(tagSplit) > 1 {
			tagValue = tagSplit[1]
		}
		ts[tagSplit[0]] = tagValue
	}

	return ts
}
