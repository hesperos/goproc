# goproc - procfiles encoder/decoder

`goproc` is a small module aiming to simplify dealing with Linux's `/proc`
files.  It attempts to provide a similar interface to JSON
marshaller/unmarshaller.  The `/proc` file's schema is defined using golang's
native structures.

## Usage

Decoding a `/proc` file example:

```golang
func PrintUptime() error {
	uptime := &goproc.Uptime{}

	fh, err := os.Open(uptime.Name())
	if err != nil {
		return err
	}

	decoder, err := goproc.NewDecoder(fh)
	if err != nil {
		return err
	}

	if err := decoder.Decode(uptime); err != nil {
		return err
	}

	fmt.Println("Uptime:", uptime.Uptime)
	return nil
}
```

Please, refer to [cmd/](https://gitlab.com/hesperos/goproc/-/tree/master/cmd) for more thorough examples.

## Read more about it

You can read more about this module on my [blog](https://twdev.blog/2022/06/go_proc/)
