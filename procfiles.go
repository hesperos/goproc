package goproc

type Named interface {
	Name() string
}

// Single line stored in /proc/mounts
type MountInfo struct {
	Device     string
	MountPoint string
	Filesystem string
	Options    string
	Dumpable   int
	FsckOrder  int
}

// Data stored in /proc/mounts
type Mounts []MountInfo

func (m Mounts) Name() string {
	return "/proc/mounts"
}

// Data stored in /proc/loadavg
type Loadavg struct {
	OneMin     float64
	FiveMin    float64
	FifteenMin float64
	RunSched   string
	LatestPid  int
}

func (l *Loadavg) Name() string {
	return "/proc/loadavg"
}

// Data stored in /proc/uptime
type Uptime struct {
	Uptime float64
	Idle   float64
}

func (u *Uptime) Name() string {
	return "/proc/uptime"
}

type VmstatInfo struct {
	Key   string
	Value int
}

type Vmstat []VmstatInfo

func (v Vmstat) Name() string {
	return "/proc/vmstat"
}

// Single line stored in /proc/partitions
type PartitionInfo struct {
	Major  int
	Minor  int
	Blocks int
	Name   string
}

// Data stored in /proc/partitions
type Partitions struct {
	title          string `goproc:"omit:4"`
	PartitionsInfo []PartitionInfo
}

func (p *Partitions) Name() string {
	return "/proc/partitions"
}
