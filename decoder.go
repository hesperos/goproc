package goproc

import (
	"bufio"
	"errors"
	"io"
	"reflect"
	"strconv"
)

var (
	ErrUnsupportedFieldType = errors.New("Field type is not supported by parser")
)

type Decoder struct {
	wordScanner *bufio.Scanner
}

func assignFloat(v reflect.Value, fieldValue string, k reflect.Kind) error {
	bits := 32
	if k == reflect.Float64 {
		bits = 64
	}

	floatVal, err := strconv.ParseFloat(fieldValue, bits)
	if err != nil {
		return err
	}
	v.SetFloat(floatVal)
	return nil
}

func (d *Decoder) scanNext(k reflect.Kind) (string, error) {
	// determine if scanning is required at this point
	tokensNeedingValue := map[reflect.Kind]bool{
		reflect.Bool:          false,
		reflect.Int:           false,
		reflect.Int8:          false,
		reflect.Int16:         false,
		reflect.Int32:         false,
		reflect.Int64:         false,
		reflect.Uint:          false,
		reflect.Uint8:         false,
		reflect.Uint16:        false,
		reflect.Uint32:        false,
		reflect.Uint64:        false,
		reflect.Uintptr:       false,
		reflect.Float32:       false,
		reflect.Float64:       false,
		reflect.Pointer:       false,
		reflect.String:        false,
		reflect.UnsafePointer: false,
	}

	if _, ok := tokensNeedingValue[k]; ok {
		if !d.wordScanner.Scan() {
			return "", io.EOF
		}
	}

	return d.wordScanner.Text(), nil
}

func (d *Decoder) handleOmitTag(tagString string) (bool, error) {
	tags := fromTagString(tagString)
	if value, ok := tags["omit"]; ok {
		n := 1
		if len(value) > 0 {
			skips, err := strconv.Atoi(value)
			if err != nil {
				return false, err
			}
			n = skips
		}

		// skip n fields from input
		if err := d.omitFields(n); err != nil {
			return false, err
		}

		return true, nil
	}

	return false, nil
}

func (d *Decoder) omitFields(n int) error {
	for n > 0 {
		if !d.wordScanner.Scan() {
			return io.EOF
		}
		n -= 1
	}
	return nil
}

func (d *Decoder) Decode(v interface{}) error {
	t := reflect.TypeOf(v)
	if t.Kind() != reflect.Ptr {
		return errors.New("expected a pointer type")
	}

	// dereferenced type
	tv := t.Elem()

	// pointer to value
	pv := reflect.ValueOf(v)

	// dereferenced value
	pve := pv.Elem()

	// fetch next field value if needed
	fieldValue, err := d.scanNext(pve.Kind())
	if err != nil {
		return err
	}

	// determine the pointer type
	switch pvek := pve.Kind(); pvek {
	case reflect.Slice:
		elemType := tv.Elem()
		for {
			elem := reflect.New(elemType)
			if err := d.Decode(elem.Interface()); err != nil {
				if err == io.EOF {
					break
				}
				return err
			}
			pve.Set(reflect.Append(pve, elem.Elem()))
		}

	case reflect.Float32:
		fallthrough

	case reflect.Float64:
		if err := assignFloat(pve, fieldValue, pvek); err != nil {
			return err
		}

	case reflect.Int:
		intVal, err := strconv.ParseInt(fieldValue, 10, 64)
		if err != nil {
			return err
		}
		pve.SetInt(intVal)

	case reflect.String:
		pve.SetString(fieldValue)

	case reflect.Struct:
	FieldLoop:
		for i := 0; i < pve.NumField(); i++ {
			fv := pve.Field(i)
			ft := tv.Field(i)

			sf := reflect.StructField(ft)
			tagValue, ok := sf.Tag.Lookup("goproc")
			if ok {
				skipField, err := d.handleOmitTag(tagValue)
				if err != nil {
					return err
				}

				if skipField {
					// proceed to next field
					continue FieldLoop
				}
			}

			// recursively descend and assign value to field
			if err := d.Decode(fv.Addr().Interface()); err != nil {
				return err
			}
		}

	default:
		return ErrUnsupportedFieldType
	} // switch

	return nil
}

func NewDecoder(r io.Reader) (*Decoder, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanWords)
	return &Decoder{
		wordScanner: scanner,
	}, nil
}
