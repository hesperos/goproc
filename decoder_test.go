package goproc_test

import (
	"bufio"
	"strings"
	"testing"

	is "github.com/matryer/is"
	"gitlab.com/hesperos/goproc"
)

func TestMountsDecoder(t *testing.T) {
	is := is.New(t)

	m := goproc.Mounts{}

	data := `proc /proc proc rw,nosuid,nodev,noexec,relatime 0 0
sys /sys sysfs rw,nosuid,nodev,noexec,relatime 0 0
dev /dev devtmpfs rw,nosuid,relatime,size=8138248k,nr_inodes=2034562,mode=755,inode64 0 0
run /run tmpfs rw,nosuid,nodev,relatime,mode=755,inode64 0 0
/dev/mapper/mimir--vg-root / ext4 rw,relatime 0 0
securityfs /sys/kernel/security securityfs rw,nosuid,nodev,noexec,relatime 0 0
tmpfs /dev/shm tmpfs rw,nosuid,nodev,inode64 0 0
devpts /dev/pts devpts rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000 0 0
cgroup2 /sys/fs/cgroup cgroup2 rw,nosuid,nodev,noexec,relatime,nsdelegate,memory_recursiveprot 0 0
pstore /sys/fs/pstore pstore rw,nosuid,nodev,noexec,relatime 0 0
bpf /sys/fs/bpf bpf rw,nosuid,nodev,noexec,relatime,mode=700 0 0
systemd-1 /proc/sys/fs/binfmt_misc autofs rw,relatime,fd=30,pgrp=1,timeout=0,minproto=5,maxproto=5,direct,pipe_ino=13842 0 0
hugetlbfs /dev/hugepages hugetlbfs rw,relatime,pagesize=2M 0 0
mqueue /dev/mqueue mqueue rw,nosuid,nodev,noexec,relatime 0 0
debugfs /sys/kernel/debug debugfs rw,nosuid,nodev,noexec,relatime 0 0
tracefs /sys/kernel/tracing tracefs rw,nosuid,nodev,noexec,relatime 0 0
configfs /sys/kernel/config configfs rw,nosuid,nodev,noexec,relatime 0 0
fusectl /sys/fs/fuse/connections fusectl rw,nosuid,nodev,noexec,relatime 0 0
ramfs /run/credentials/systemd-sysusers.service ramfs ro,nosuid,nodev,noexec,relatime,mode=700 0 0
/dev/sda1 /boot ext2 rw,relatime 0 0
tmpfs /tmp tmpfs rw,nosuid,nodev,size=8153136k,nr_inodes=1048576,inode64 0 0
/dev/mapper/mimir--vg-home /home ext4 rw,relatime 0 0
tmpfs /run/user/1000 tmpfs rw,nosuid,nodev,relatime,size=1630624k,nr_inodes=407656,mode=700,uid=1000,gid=1000,inode64 0 0
gvfsd-fuse /run/user/1000/gvfs fuse.gvfsd-fuse rw,nosuid,nodev,relatime,user_id=1000,group_id=1000 0 0
portal /run/user/1000/doc fuse.portal rw,nosuid,nodev,relatime,user_id=1000,group_id=1000 0 0 `

	r := strings.NewReader(data)
	dec, err := goproc.NewDecoder(r)
	is.NoErr(err)
	err = dec.Decode(&m)
	is.NoErr(err)

	is.Equal(25, len(m))

	scanner := bufio.NewScanner(strings.NewReader(data))
	scanner.Split(bufio.ScanLines)

	idx := 0
	for scanner.Scan() {
		text := scanner.Text()
		firstWord := strings.Split(text, " ")[0]
		firstWord = strings.TrimSpace(firstWord)
		is.Equal(firstWord, m[idx].Device)
		idx++
	}
}

func TestLoadavgDecoder(t *testing.T) {
	is := is.New(t)

	l := goproc.Loadavg{}

	data := `0.20 0.30 0.34 1/528 29071`
	r := strings.NewReader(data)

	dec, err := goproc.NewDecoder(r)
	is.NoErr(err)
	err = dec.Decode(&l)
	is.NoErr(err)

	is.Equal(0.2, l.OneMin)
	is.Equal(0.3, l.FiveMin)
	is.Equal(0.34, l.FifteenMin)
	is.Equal("1/528", l.RunSched)
	is.Equal(29071, l.LatestPid)
}

func TestUptimeDecoder(t *testing.T) {
	is := is.New(t)

	u := goproc.Uptime{}

	data := `4647.35 17260.42`
	r := strings.NewReader(data)

	dec, err := goproc.NewDecoder(r)
	is.NoErr(err)
	err = dec.Decode(&u)
	is.NoErr(err)

	is.Equal(4647.35, u.Uptime)
	is.Equal(17260.42, u.Idle)
}

func TestVmstatDecoder(t *testing.T) {
	is := is.New(t)

	v := goproc.Vmstat{}

	data := `htlb_buddy_alloc_success 0
htlb_buddy_alloc_fail 0
cma_alloc_success 0
cma_alloc_fail 0
unevictable_pgs_culled 614398
unevictable_pgs_scanned 510953
unevictable_pgs_rescued 510928
unevictable_pgs_mlocked 44
unevictable_pgs_munlocked 24
unevictable_pgs_cleared 0
unevictable_pgs_stranded 0
thp_fault_alloc 0
thp_fault_fallback 0
thp_fault_fallback_charge 0
thp_collapse_alloc 0
thp_collapse_alloc_failed 0
thp_file_alloc 0
thp_file_fallback 0
thp_file_fallback_charge 0
thp_file_mapped 0
thp_split_page 0
thp_split_page_failed 0
thp_deferred_split_page 0
thp_split_pmd 1
thp_split_pud 0
thp_zero_page_alloc 1
thp_zero_page_alloc_failed 0
thp_swpout 0
thp_swpout_fallback 0
balloon_inflate 0
balloon_deflate 0
balloon_migrate 0
swap_ra 0
swap_ra_hit 0
direct_map_level2_splits 103
direct_map_level3_splits 2
nr_unstable 0`

	r := strings.NewReader(data)

	dec, err := goproc.NewDecoder(r)
	is.NoErr(err)
	err = dec.Decode(&v)
	is.NoErr(err)

	is.Equal(37, len(v))
}

func TestPartitionsDecoder(t *testing.T) {
	is := is.New(t)

	p := goproc.Partitions{}

	data := `major minor  #blocks  name

   8        0  125034840 sda
   8        1     248832 sda1
   8        2          1 sda2
   8        5  124783616 sda5
 254        0   29294592 dm-0
 254        1   16695296 dm-1
 254        2   78790656 dm-2
`
	r := strings.NewReader(data)

	dec, err := goproc.NewDecoder(r)
	is.NoErr(err)
	err = dec.Decode(&p)
	is.NoErr(err)

	is.Equal(7, len(p.PartitionsInfo))
}
