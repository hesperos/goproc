package goproc_test

import (
	"strings"
	"testing"

	is "github.com/matryer/is"
	"gitlab.com/hesperos/goproc"
)

func TestSimpleStruct(t *testing.T) {
	is := is.New(t)
	a := struct {
		X int
		Y float64
		Z string
	}{
		123, 3.14, "hello world",
	}
	expected := "123 3.1400000000 hello world\n"

	b := &strings.Builder{}
	enc, err := goproc.NewEncoder(b)
	is.NoErr(err)

	err = enc.Encode(&a)
	is.NoErr(err)

	is.Equal(len(expected), len(b.String()))
	is.Equal(expected, b.String())
}

func TestStructSlice(t *testing.T) {
	is := is.New(t)

	type Given struct {
		X int
		Y float64
		Z string
	}

	data := []Given{
		{123, 3.14, "first row"},
		{456, 2.78, "second row"},
	}

	expected := "123 3.1400000000 first row\n456 2.7800000000 second row\n"

	b := &strings.Builder{}
	enc, err := goproc.NewEncoder(b)
	is.NoErr(err)

	err = enc.Encode(&data)
	is.NoErr(err)
	is.Equal(expected, b.String())
	is.Equal(len(expected), len(b.String()))
}
