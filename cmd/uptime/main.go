package main

import (
	"fmt"
	"os"

	"gitlab.com/hesperos/goproc"
)

func PrintUptime() error {
	uptime := &goproc.Uptime{}

	fh, err := os.Open(uptime.Name())
	if err != nil {
		return err
	}

	decoder, err := goproc.NewDecoder(fh)
	if err != nil {
		return err
	}

	if err := decoder.Decode(uptime); err != nil {
		return err
	}

	fmt.Println("Uptime:", uptime.Uptime)
	return nil
}

func main() {
	PrintUptime()
}
