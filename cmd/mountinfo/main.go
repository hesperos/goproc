package main

import (
	"fmt"
	"log"
	"os"
	"text/tabwriter"

	"gitlab.com/hesperos/goproc"
)

func main() {
	m := goproc.Mounts{}

	f, err := os.Open(m.Name())
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	decoder, err := goproc.NewDecoder(f)
	if err != nil {
		log.Fatalln(err)
	}

	err = decoder.Decode(&m)
	if err != nil {
		log.Fatalln(err)
	}

	minWidth := 4
	tabWidth := 2
	padding := 2
	padchar := byte(' ')
	tw := tabwriter.NewWriter(os.Stdout, minWidth, tabWidth, padding, padchar, 0)

	fmt.Fprintln(tw,
		"Device\tMountPoint\tFilesystem\tDumpable\tFsckOrder")

	for _, mi := range m {
		fmt.Fprintf(tw, "%s\t%s\t%s\t%d\t%d\n",
			mi.Device,
			mi.MountPoint,
			mi.Filesystem,
			mi.Dumpable,
			mi.FsckOrder)
	}

	if err := tw.Flush(); err != nil {
		log.Fatalln(err)
	}
}
