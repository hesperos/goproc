package goproc

import (
	"bufio"
	"io"
	"reflect"
	"strconv"
)

type Encoder struct {
	w *bufio.Writer
}

func (d *Encoder) Encode(v interface{}) error {
	// pointer to value
	pv := reflect.ValueOf(v)

	// dereferenced value
	pve := pv.Elem()

	switch pvek := pve.Kind(); pvek {
	case reflect.Slice:
		for i := 0; i < pve.Len(); i++ {
			fv := pve.Index(i)
			if err := d.Encode(fv.Addr().Interface()); err != nil {
				return err
			}
		}

	case reflect.Int:
		d.w.WriteString(strconv.FormatInt(pve.Int(), 10))

	case reflect.Float32:
		fallthrough
	case reflect.Float64:
		d.w.WriteString(strconv.FormatFloat(pve.Float(), 'f', 10, 64))

	case reflect.String:
		d.w.WriteString(pve.String())

	case reflect.Struct:
		for i := 0; i < pve.NumField(); i++ {
			fv := pve.Field(i)

			if err := d.Encode(fv.Addr().Interface()); err != nil {
				return err
			}

			if i < pve.NumField()-1 {
				d.w.WriteRune(' ')
			}
		}
		d.w.WriteRune('\n')
	}

	return d.w.Flush()
}

func NewEncoder(w io.Writer) (*Encoder, error) {
	return &Encoder{
		w: bufio.NewWriter(w),
	}, nil
}
